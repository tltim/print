
## 项目介绍
用于标签远程打印应用
客户服务生成对应的html网页调用本服务，设定对应的打印机数据，进行打印


## 使用教程

安装 lib 文件夹下的
sumatraPDF和 wkhtmltox 应用,配置对应软件可执行文件到配置文件中，推荐奖路径加到windows 的path中进行配置
```yaml
wkhtmltopdf:
  path: 'wkhtmltopdf.exe'
SumatraPdf:
  path: 'C:\Users\wfu7326\AppData\Local\SumatraPDF\SumatraPDF.exe'
tmp:
  path: 'D:\tmp\'
```

启动项目

```
http://localhost:8080/api/print/url

{
    "printerId":"1",
    "url": "http://www.baidu.com",
    "num":"1"
}
```
