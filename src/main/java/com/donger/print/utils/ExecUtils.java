package com.donger.print.utils;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.IdUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.charset.Charset;

@Slf4j
@Component
public class ExecUtils {


    @Value("${SumatraPdf.path}")
    private String SumatraPdfPath;

    @Value("${wkhtmltopdf.path}")
    private String wkhtmltopdfPath;

    @Value("${tmp.path}")
    private String tmpPath;


    public String generatePdf(String url) {
        String tmpFile = tmpPath + IdUtil.simpleUUID() + ".pdf";
        this.generatePdf(url, tmpFile);
        return tmpFile;
    }

    /**
     * wkhtmltopdf
     */
    @SneakyThrows
    public void generatePdf(String url, String tmpFile) {


        String printStr = "%s %s %s";
        String microsoft_print_to_pdf = String.format(printStr, wkhtmltopdfPath, url, tmpFile);
        log.info("打印执行的参数 => {}", microsoft_print_to_pdf);
        Process exec = Runtime.getRuntime().exec(microsoft_print_to_pdf);

        new Thread(() -> {
            String read = IoUtil.read(exec.getInputStream(), Charset.forName("GBK"));
            log.info("log success -> {}", read);
        }).start();
        new Thread(() -> {
            String read = IoUtil.read(exec.getErrorStream(), Charset.forName("GBK"));
            log.info("log error -> {}", read);
        }).start();
        exec.waitFor();
    }


    @SneakyThrows
    public void pdfPrint(String deviceName, Integer num, String dir) {

        /**
         * 打印机名称
         * 设置
         * 路径
         */
        String printStr = "\"%s\" -print-to \"%s\" -print-settings %dx \"%s\"";
        String microsoft_print_to_pdf = String.format(printStr, SumatraPdfPath, deviceName, num, dir);
        log.info("打印执行的参数 => {}", microsoft_print_to_pdf);
        Process exec = Runtime.getRuntime().exec(microsoft_print_to_pdf);
        new Thread(() -> {
            String read = IoUtil.read(exec.getInputStream(), Charset.forName("GBK"));
            log.info("log -> {}", read);
        }).start();
        new Thread(() -> {
            String read = IoUtil.read(exec.getErrorStream(), Charset.forName("GBK"));
            log.info("log -> {}", read);
        }).start();
        exec.waitFor();
    }


    /**
     * 奖字符串生成对应的html 文件 用于打印
     * @param content
     * @return
     */
    @SneakyThrows
    public String genPdf(String content) {

        String htmlFile = tmpPath + IdUtil.simpleUUID() + ".html";
        File tempFile = FileUtil.file(htmlFile);

        BufferedOutputStream out = FileUtil.getOutputStream(tempFile);
        ByteArrayInputStream byteArrayInputStream = IoUtil.toUtf8Stream(content);

        long copy = IoUtil.copy(byteArrayInputStream, out);


        String pdfFile = tmpPath + IdUtil.simpleUUID() + ".pdf";
        String printStr = "%s %s %s";
        String microsoft_print_to_pdf = String.format(printStr, wkhtmltopdfPath, tempFile.getPath(), pdfFile);
        log.info("打印执行的参数 => {}", microsoft_print_to_pdf);
        Process exec = Runtime.getRuntime().exec(microsoft_print_to_pdf);
        new Thread(() -> {
            String read = IoUtil.read(exec.getInputStream(), Charset.forName("GBK"));
            log.info("log -> {}", read);
        }).start();
        new Thread(() -> {
            String read = IoUtil.read(exec.getErrorStream(), Charset.forName("GBK"));
            log.info("log -> {}", read);
        }).start();
        exec.waitFor();

        boolean delete = tempFile.delete();

        return pdfFile;
    }

}
