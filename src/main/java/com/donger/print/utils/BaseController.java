package com.donger.print.utils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public class BaseController<T extends IService, M> {

    @Autowired
    protected T baseService;


    @GetMapping("/page")
    public Result page(Page<M> page, M admin, CommonQuery commonQuery) {
        QueryWrapper<M> wrapper = QueryGenerator.initQueryWrapper(admin, commonQuery);
        baseService.page(page, wrapper);
        return Res.ok(page);
    }


    /**
     * 用户信息
     */
    @GetMapping("/info/{id}")
    @ApiOperation(value = "根据用户id查询用户信息")
    public Result<M> info(@ApiParam(value = "用户id") @PathVariable("id") Long id) {
        M user = (M) baseService.getById(id);
        return Res.ok(user);
    }


    /**
     * 保存用户
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增用户")
    public Result<Boolean> save(@RequestBody M entity) {
        return Res.ok(baseService.save(entity));
    }

    /**
     * 修改用户
     */
    @PostMapping("/update")
    @ApiOperation(value = "更新用户")
    public Result<Boolean> update(@RequestBody M entity) {
        return Res.ok(baseService.updateById(entity));
    }

    /**
     * 删除用户
     */
    @GetMapping("/delete/{id}")
    @ApiOperation(value = "删除用户")
    public Result<Boolean> delete(@ApiParam(value = "用户id") @PathVariable("id") Long id) {
        return Res.ok(baseService.removeById(id));
    }


}
