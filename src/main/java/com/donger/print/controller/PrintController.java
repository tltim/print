package com.donger.print.controller;


import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.print.entity.PrintObj;
import com.donger.print.service.PrintService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/print")
@RestController
@Slf4j
@AllArgsConstructor
public class PrintController {

    private final PrintService printService;


    /**
     * 根据url 进行打印
     *
     * @return
     */
    @SneakyThrows
    @PostMapping("/url")
    public Result printByUrl(@RequestBody PrintObj printObj) {
        //
        printService.printByUrl(printObj);
        return Res.ok();
    }


    /**
     * 根据url 进行打印
     *
     * @return
     */
    @SneakyThrows
    @PostMapping("/template")
    public Result printByTemplate(@RequestBody PrintObj printObj) {
        //
        printService.printByTemplate(printObj);
        return Res.ok();
    }

}
