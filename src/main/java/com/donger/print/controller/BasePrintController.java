package com.donger.print.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.print.entity.BasePrint;
import com.donger.print.enums.StatusEnums;
import com.donger.print.service.BasePrintService;
import com.donger.print.utils.BaseController;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/print")
@RestController
@AllArgsConstructor
public class BasePrintController extends BaseController<BasePrintService,BasePrint> {



    /**
     * 查询打印机信息
     *
     * @return
     */
    @GetMapping("/list")
    public Result list() {

        List<BasePrint> list = baseService.list(Wrappers.<BasePrint>lambdaQuery().eq(BasePrint::getStatus, StatusEnums.activate.getType()));
        return Res.ok(list);
    }
}
