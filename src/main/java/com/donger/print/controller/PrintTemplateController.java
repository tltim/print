package com.donger.print.controller;


import com.donger.print.entity.PrintObj;
import com.donger.print.entity.PrintTemplate;
import com.donger.print.service.PrintService;
import com.donger.print.service.PrintTemplateService;
import com.donger.print.utils.BaseController;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/template")
@AllArgsConstructor
public class PrintTemplateController extends BaseController<PrintTemplateService, PrintTemplate> {

    private final PrintService printService;


    /**
     * 预览生成的模板内容
     *
     * @param printObj
     * @return
     */
    @PostMapping("/preview")
    public String templateData(@RequestBody PrintObj printObj) {
        String s = printService.genTemplate(printObj);
        return s;
    }

}
