package com.donger.print.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.print.entity.BasePrint;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BasePrintMapper extends BaseMapper<BasePrint> {
}
