package com.donger.print.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.print.entity.PrintTemplate;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PrintTemplateMapper extends BaseMapper<PrintTemplate> {
}
