package com.donger.print.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusEnums {

    activate("1","激活"),
    Deactivate("0","停用");


    /**
     * 类型
     */
    private String type;
    /**
     * 描述
     */
    private String description;
}
