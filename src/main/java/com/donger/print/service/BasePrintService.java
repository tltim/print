package com.donger.print.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.donger.print.entity.BasePrint;

public interface BasePrintService extends IService<BasePrint> {
}
