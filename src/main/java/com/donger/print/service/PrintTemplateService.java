package com.donger.print.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.donger.print.entity.PrintTemplate;

public interface PrintTemplateService extends IService<PrintTemplate> {
}
