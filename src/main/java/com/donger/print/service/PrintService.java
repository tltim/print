package com.donger.print.service;

import com.donger.print.entity.PrintObj;

public interface PrintService {


    /**
     * 根据url 打印
     * @param printObj
     */
    void printByUrl(PrintObj printObj);


    /**
     * 根据模板打印
     * @param printObj
     */
    void printByTemplate(PrintObj printObj);


    String genTemplate(PrintObj printObj);
}
