package com.donger.print.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.extra.template.Template;
import cn.hutool.extra.template.TemplateConfig;
import cn.hutool.extra.template.TemplateEngine;
import cn.hutool.extra.template.TemplateUtil;
import cn.hutool.extra.template.engine.TemplateFactory;
import cn.hutool.json.JSONUtil;
import com.donger.print.entity.BasePrint;
import com.donger.print.entity.PrintObj;
import com.donger.print.entity.PrintTemplate;
import com.donger.print.enums.StatusEnums;
import com.donger.print.service.BasePrintService;
import com.donger.print.service.PrintService;
import com.donger.print.service.PrintTemplateService;
import com.donger.print.utils.BizException;
import com.donger.print.utils.ExecUtils;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Optional;


@Service
@AllArgsConstructor
public class PrintServiceImpl implements PrintService {
    private final ExecUtils execUtils;
    private final BasePrintService basePrintService;

    private final PrintTemplateService printTemplateService;


    @Override
    public void printByUrl(PrintObj printObj) {

        BasePrint basePrint = Optional.ofNullable(basePrintService.getById(printObj.getPrinterId()))
                .orElseThrow(() -> new BizException("打印机不存在"));
        if(!StatusEnums.activate.getType().equals(basePrint.getStatus())){
            throw new BizException("打印机状态不正确");
        }

        String tmpFile = execUtils.generatePdf(printObj.getUrl());
        execUtils.pdfPrint(basePrint.getSystemName(),printObj.getNum(),tmpFile);
        // 打印完成删除文件
        FileUtil.del(tmpFile);
    }

    /**
     * 按照模板打印
     * @param printObj
     */
    @Override
    public void printByTemplate(PrintObj printObj) {

        BasePrint basePrint = Optional.ofNullable(basePrintService.getById(printObj.getPrinterId()))
                .orElseThrow(() -> new BizException("打印机不存在"));

        // 生成模板字符串
        String result = this.genTemplate(printObj);


        String s = execUtils.genPdf(result);
        execUtils.pdfPrint(basePrint.getSystemName(),printObj.getNum(),s);
        // 打印完成删除文件
//        FileUtil.del(s);


    }

    /**
     * 生成模板引擎
     * @param printObj
     * @return
     */
    @Override
    public String genTemplate(PrintObj printObj){


        PrintTemplate printTemplate = printTemplateService.getById(printObj.getTemplateId());
        TemplateConfig templateConfig = new TemplateConfig();
        TemplateEngine engine = TemplateUtil.createEngine(templateConfig);

        //假设我们引入的是Beetl引擎，则：
        Template template = engine.getTemplate(printTemplate.getContent());

        HashMap hashMap = JSONUtil.parse(printObj.getTemplateData()).toBean(HashMap.class);

        //Dict本质上为Map，此处可用Map
        String result = template.render(hashMap);
        return result;
    }
}
