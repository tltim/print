package com.donger.print.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.print.entity.BasePrint;
import com.donger.print.mapper.BasePrintMapper;
import com.donger.print.service.BasePrintService;
import org.springframework.stereotype.Service;

@Service
public class BasePrintServiceImpl extends ServiceImpl<BasePrintMapper, BasePrint> implements BasePrintService {
}
