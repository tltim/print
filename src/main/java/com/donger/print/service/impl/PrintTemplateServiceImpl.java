package com.donger.print.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.print.entity.PrintTemplate;
import com.donger.print.mapper.PrintTemplateMapper;
import com.donger.print.service.PrintTemplateService;
import org.springframework.stereotype.Service;

@Service
public class PrintTemplateServiceImpl extends ServiceImpl<PrintTemplateMapper, PrintTemplate> implements PrintTemplateService {
}
