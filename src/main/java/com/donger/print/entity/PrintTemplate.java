package com.donger.print.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.DefaultValue;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

/**
 * 模板管理
 */
@TableComment("模板管理")
@TableName
@Data
public class PrintTemplate {

    @TableId
    @ColumnComment("id")
    private Long id;

    @TableField
    @ColumnComment("模板内容")
    private String content;

    @TableField
    @ColumnComment("模板标题")
    private String title;

    @TableField
    @ColumnComment("备注")
    private String remarks;


    /**
     * 状态 1 可用 0 不可用
     */
    @TableField
    @ColumnComment("状态")
    @DefaultValue("1")
    private String status;
}
