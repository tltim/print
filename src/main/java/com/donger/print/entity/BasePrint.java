package com.donger.print.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.DefaultValue;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

@Data
@TableName
@TableComment("打印机设备")
public class BasePrint {
    @TableId
    @ColumnComment("id")
    private Long id;
    /**
     * 打印机名称
     */
    @TableField
    @ColumnComment("打印机名称")
    private String name;
    /**
     * 地址
     */
    @TableField
    @ColumnComment("地址")
    private String netAddress;
    /**
     * 打印机系统名称
     */
    @TableField
    @ColumnComment("打印机系统名称")
    private String systemName;
    /**
     * 页面大小
     */
    @TableField
    @ColumnComment("页面大小")
    private String pageSize;

    /**
     * 状态
     */
    @TableField
    @ColumnComment("状态")
    @DefaultValue("1")
    private String status;
}
