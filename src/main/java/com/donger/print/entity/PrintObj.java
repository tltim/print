package com.donger.print.entity;

import lombok.Data;

@Data
public class PrintObj {
    /**
     * 打印地址
     */
    private String url;

    /**
     * 打印机id
     */
    private String printerId;

    /**
     * 打印数量
     */
    private Integer num;

    /**
     * 模板id
     */
    private Long templateId;


    /**
     * 模板中填充的数据
     */
    private String templateData;
}
